 
# minecraft:tick

# tag an entity as "MOC_follower" to make it use this behavior (tested only on armor stands)
# tag a player as "MOC_followable" to make them chasable

# comment the line under this to make all players unfollowable by default
tag @a[tag=!MOC_noautofollow] add MOC_followable

# at any point you can use "/function mocchapi_followers:followme" and "/function mocchapi_followers:dontfollowme" to enable and disable followers following you specifically
# note that you do need to have OP for this


# general movement/unstuckyness
execute as @e[tag=MOC_follower] at @s run execute unless block ~ ~ ~ air run tp ~ ~.2 ~
execute as @e[tag=MOC_follower] at @s run execute if block ~ ~-.2 ~ air unless block ~ ~-2 ~ air unless block ~ ~-1 ~ air run tp ~ ~-.2 ~



# normal follow
execute as @e[tag=MOC_follower] at @s run execute unless entity @a[tag=MOC_followable,sort=nearest,limit=1,distance=..18] unless block ~ ~-.2 ~ air if block ~ ~ ~ air run tp @s ^ ^.1 ^.3 facing entity @a[tag=MOC_followable,sort=nearest,limit=1,distance=11..32]

# look at nearest player
execute as @e[tag=MOC_follower] at @s run execute unless entity @a[tag=MOC_followable,sort=nearest,limit=1,distance=..6] unless block ~ ~-.2 ~ air if block ~ ~ ~ air run tp @s ~ ~ ~ facing entity @a[sort=nearest,limit=1,distance=6..18, tag=MOC_followable]

# TP to player when out of range
# original
#execute as @e[tag=MOC_follower] at @s run execute unless entity @a[tag=MOC_followable,sort=nearest,limit=1,distance=..32] run execute rotated as @a[sort=furthest,limit=1, tag=MOC_followable, distance=32..] positioned as @a[sort=furthest,limit=1,distance=32.., tag=MOC_followable] run tp ^ ^ ^-28
# alternative to avoid collisions
execute as @e[tag=MOC_follower] at @s run execute unless entity @a[tag=MOC_followable,sort=nearest,limit=1,distance=..32] run execute rotated as @a[sort=nearest,limit=1, tag=MOC_followable, distance=32..] positioned as @a[sort=nearest,limit=1,distance=32.., tag=MOC_followable] run execute positioned ^ ^ ^-23 run spreadplayers ~ ~ 5 10 false @s



# back away
#  v look at player when backing away
#execute as @e[tag=MOC_follower] at @s run execute unless block ~ ~-.2 ~ air if block ~ ~ ~ air if block ^ ^ ^-.2 air run tp @s ^ ^.1 ^-.2 facing entity @a[sort=nearest,limit=1,distance=0..5, tag=MOC_followable,]
#  v look away from player when backing away
execute as @e[tag=MOC_follower] at @s run execute if entity @a[tag=MOC_followable,sort=nearest,limit=1,distance=..6] if block ~ ~ ~ air unless block ~ ~-2 ~ air run execute rotated as @a[sort=nearest,limit=1,distance=..6, tag=MOC_followable] run tp @s ^ ^.1 ^.3 facing ^ ^ ^20

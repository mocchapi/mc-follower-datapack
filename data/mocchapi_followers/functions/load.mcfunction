 
 # minecraft:load
tellraw @a[gamemode=creative] "followers V0.1b7 is loaded."
execute unless entity @e[tag=MOC_follower] run tellraw @a "followers: spawned new prefab since none is alive. You can disable this by commenting the last line in load.mcfunction"
execute unless entity @e[tag=MOC_follower] run execute at @r run summon minecraft:armor_stand ^ ^1 ^3 {Tags:["MOC_follower"], ShowArms:1b, NoBasePlate:1b, Invulnerable:1b, ArmorItems: [{id:"minecraft:leather_boots", Count:1b, tag:{Damage:0, display: {color:1908001}}}, {id:"minecraft:leather_leggings", Count:1b, tag:{Damage:0, display: {color:16383998}}}, {id:"minecraft:leather_chestplate", Count:1b, tag:{Damage:0, display: {color:10082540}}}, {id:"minecraft:player_head", Count:1b, tag:{SkullOwner:"moccc"}}]}